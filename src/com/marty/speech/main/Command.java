package com.marty.speech.main;



public class Command {
	private String phrase="";
	private String command="";

	public Command(String phrase,String command)
	{
		this.phrase=phrase;
		this.command=command;
	}
	
	public Command()
	{
		
	}
	
	public String getPhrase()
	{
		return this.phrase;
	}
	
	public void setPhrase(String ph)
	{
		this.phrase=ph;
	}
	
	public String getCommand()
	{
		return this.command;
	}
	
	public void setCommand(String command)
	{
		this.command=command;
	}
	
	public String runCommand(String phrase) throws Exception
	{
		String ph=new String(this.phrase.getBytes("UTF-8"),"UTF-8");
		
		if(phrase.equals(ph))
		{
			ProcessBuilder builder=new ProcessBuilder("cmd.exe","/c",this.command);
			Process process=builder.start();
						
			return "Команда "+this.command+" успешно выполнена!";
		}
		else 
		{
			return "...";
		}
	}
}
