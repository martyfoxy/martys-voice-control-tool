/*//////////////////////
//Marty's voice Recognition Tool v2
//Создал: Чудаев 'MartyFoxy' Вячеслав
//Испольнованы сторонние классы(OpenSource): DarkPrograms.speech.microphone, javaFlacEncoder
//Последнее обновление 22.10.2014 
//Изменения: переход к версии Speech API v2, добавление API Key, исправления с получением ответа от сервера
//Описание: после нажатия кнопки "слушать" идет 5-секундная запись(можно изменить константу MAXSECONDS) во временный .wav файл,
//          после этого идет конвертация в .flac файл, т.к. Google Speech Api работает только с .flac . Далее идет POST-запрос
//          и получение ответа от сервера. Ответом является json-файл со списком распознанных фраз. После этого идет перебор и сравнение
//          распознанной фразы с фразами, введенными пользователем и сохраненные в .xml файл. При нахождении совпавшей фразы идет
//          запуск процесса cmd.exe с параметрами, которые ввел пользователь.
*///////////////////////
package com.marty.speech.main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javaFlacEncoder.FLACEncoder;
import javaFlacEncoder.FLACFileOutputStream;
import javaFlacEncoder.StreamConfiguration;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.*;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import com.darkprograms.speech.microphone.MicrophoneAnalyzer;

public class MyFrame extends JFrame implements ActionListener
{
	//////////////////////Компоненты на фрейме///////////////////////////////
	private JButton listenButton=new JButton("Слушать"); //кнопка слушать
	private JProgressBar volumeBar=new JProgressBar(0,100); //громкость
	private JRadioButton ruslangButton=new JRadioButton("Русский",true); //выбор русского языка
	private JRadioButton englangButton=new JRadioButton("Английский"); //выбор английского языка
	private JRadioButton gerlangButton=new JRadioButton("Немецкий"); //выбор немецкого языка
	private ButtonGroup languageSelect=new ButtonGroup(); //группа радиокнопок
	private Console console=new Console(500,250); //консоль
	private JScrollPane consoleScroll=new JScrollPane(console,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); //скролл бар консоли
	private JButton addButton=new JButton("Добавить"); //кнопка добавить
	private JButton deleteButton=new JButton("Удалить"); //кнопка удалить
	private JButton saveButton=new JButton("Сохранить"); //кнопка сохранить
	private JTextField command=new JTextField(30); //поле ввода команды
	private JTextField phrase=new JTextField(30); //поле ввода фразы
	private JLabel cLabel=new JLabel("Консольная команда:"); //надпись команда
	private JLabel pLabel=new JLabel("Фраза:"); //надпись фраза
	private JButton author=new JButton("О программе"); //кнопка о программе
	////////////////////////Панели////////////////////////////////////////
	private JPanel window=new JPanel(new BorderLayout()); //общая панель
	private JPanel maingui=new JPanel(new BorderLayout()); //главная панель
	private JPanel consolePanel=new JPanel(new BorderLayout()); //Панель консоли
	private JPanel controlPanel=new JPanel(new BorderLayout()); //Панель управления
	private JPanel buttonPanel=new JPanel(new FlowLayout(FlowLayout.RIGHT)); //Панель кнопок
	private JPanel additionalPanel=new JPanel(new BorderLayout()); //Панель добавочного управления
	private JPanel radioPanel=new JPanel(new GridLayout(0,1,1,1)); //Панель радиокнопок
	private JPanel volumePanel=new JPanel(new BorderLayout()); //Панель с ползунком
	private JPanel comgui=new JPanel(new BorderLayout()); //вторичная панель
	private JPanel controlPanelc=new JPanel(new BorderLayout()); //панель управления
	private JPanel paramsPanel=new JPanel(new BorderLayout()); //панель параметров
	private JPanel textPanel=new JPanel(new GridLayout(0,1,1,1)); //панель с вводом текста
	private JPanel buttonsPanel=new JPanel(new FlowLayout()); //панель кнопок
	private JPanel labelsPanel=new JPanel(new GridLayout(0,1,1,1)); //панель надписей
	////////////////////////////////////////////////////////////////////////////////////

	private Timer timer=new Timer(); 
	private Timer timer1=new Timer();
	private MicrophoneAnalyzer microphone=new MicrophoneAnalyzer(AudioFileFormat.Type.WAVE);
	private int counter=0;
	private final short MAXSECONDS=5;
	private String language="ru-RU"; 
	////////////URL для API v1
        ////////////private static final String googleurl="https://www.google.com/speech-api/v1/recognize?xjerr=1&client=chromium";
        private final String ApiKey="AIzaSyCqUlBX5rEArNJzHFS8U9hRA0UHcIxkKZw";
        private static final String googleurl="https://www.google.com/speech-api/v2/recognize?";
        private final String XMLFilePath="commands.xml";
	private File commandsFile=new File(XMLFilePath);
	/////////////////////////////////////////////////////////////
	final List<Command> comsArray=new ArrayList<>();
	final CommandTableModel model=new CommandTableModel(comsArray);
	JTable table=new JTable(model);
	JScrollPane tableScroll=new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	///////////////////////////////////////////////////////
	
	public MyFrame(String title,int height,int weight) 
	{
		/////////////////////////////////Фрейм////////////////////////////////
		super(title);
		setBounds(100,100,height,weight);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setResizable(false);
		///////////////////////////////Панели////////////////////////////////
		add(window);
		consolePanel.setBorder(new TitledBorder("Консоль"));
		radioPanel.setBorder(new TitledBorder("Язык"));
		volumePanel.setBorder(new TitledBorder("Громкость"));
		window.add(maingui,BorderLayout.CENTER);
		window.add(comgui,BorderLayout.EAST);		
		maingui.add(consolePanel,BorderLayout.NORTH);
		maingui.add(controlPanel,BorderLayout.CENTER);
		controlPanel.add(additionalPanel,BorderLayout.NORTH);
		controlPanel.add(buttonPanel,BorderLayout.CENTER);
		additionalPanel.add(radioPanel,BorderLayout.WEST);
		additionalPanel.add(volumePanel,BorderLayout.CENTER);
		consolePanel.add(consoleScroll,BorderLayout.NORTH);
		comgui.add(controlPanelc,BorderLayout.CENTER);
		comgui.add(tableScroll,BorderLayout.NORTH);
		tableScroll.setPreferredSize(new Dimension(500,200));
		tableScroll.setBorder(new TitledBorder("Таблица команд"));
		controlPanelc.add(paramsPanel,BorderLayout.NORTH);
		controlPanelc.add(buttonsPanel,BorderLayout.CENTER);
		paramsPanel.add(textPanel,BorderLayout.EAST);
		paramsPanel.add(labelsPanel,BorderLayout.WEST);
		paramsPanel.setBorder(new TitledBorder("Новая команда:"));
		/////////////////////////////////Кнопки////////////////////////////////
		buttonPanel.add(listenButton);
		buttonPanel.add(author);
		author.addActionListener(this);
		listenButton.addActionListener(this);
		buttonsPanel.add(addButton);
		buttonsPanel.add(deleteButton);
		buttonsPanel.add(saveButton);
		addButton.addActionListener(this);
		deleteButton.addActionListener(this);
		saveButton.addActionListener(this);
		/////////////////////////////////Ползунки//////////////////////////////
		volumePanel.add(volumeBar);
		volumeBar.setStringPainted(true);
		volumeBar.setPreferredSize(new Dimension(300,10));
		/////////////////////////////////Радиокнопки///////////////////////////
		radioPanel.add(ruslangButton);
		radioPanel.add(englangButton);
		radioPanel.add(gerlangButton);
		languageSelect.add(ruslangButton); 
		languageSelect.add(englangButton);
		languageSelect.add(gerlangButton);
		ruslangButton.addActionListener(this);
		englangButton.addActionListener(this);
		gerlangButton.addActionListener(this);
		//////////////////////////////////Таблицa////////////////////////////////
		table.setFillsViewportHeight(true);	
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		///////////////////////////////////Поля/////////////////////////////////
		textPanel.add(command);
		textPanel.add(phrase);
		command.setText("");
		phrase.setText("");
		///////////////////////////////////Надписи/////////////////////////////////
		labelsPanel.add(cLabel);
		labelsPanel.add(pLabel);		
		////////////////////////////////Проверка xml-файла///////////////////////////////
		try {
                        File xmlFile=new File(XMLFilePath);
                        loadCommands(xmlFile); //прогрузить список команд
                        console.printMessage("XML-файл с командам и фразами найден!");
		} catch (Exception e1) {
                        console.printError("\tXML-файл не обнаружен или пустой!");
                        console.printError(e1.toString());
			try {
				commandsFile.createNewFile(); //создать новый файл
				console.printMessage("Новый XML-файл с командами создан!\nДобавьте несколько команд!");
			} catch (IOException e) {
                                console.printError("\tНе удалось создать XML-файл!");
				console.printError(e.toString());
			}
		}
	}
        //////////////////////////////////////Обработка событий///////////////////////////
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==listenButton) //нажатие кнопки Слушать
		{
			try
			{
				final File file=File.createTempFile("audio_temp_file", ".wav");
				file.setWritable(true);
				file.deleteOnExit();    
				final File flacfile=File.createTempFile("audio_temp_file", ".flac");                                
                                flacfile.setWritable(true);
				flacfile.deleteOnExit();

				counter=0; //счетчик
				listenButton.setEnabled(false);
				
				timer1.scheduleAtFixedRate(new TimerTask() //каждые 100 миллисекунд 
				{
					public void run()
					{
						int volume=microphone.getAudioVolume(1); //получаем громкость с микрофона
						if(volume!=-1)
							volumeBar.setValue(volume);
							
					}
				}, 0,1);
				
				timer.schedule(new TimerTask() //после прохождения MAXSECONDS секунд
				{
					public void run()
					{
						microphone.close(); //приостановить запись
						console.printMessage("Запись завершена!");
						timer.cancel(); 
						timer=new Timer();
						volumeBar.setValue(0);
						//////////////////////
						console.printText("Конвертирую запись в .FLAC формат...");
						try {
							convertWaveToFlac(file,flacfile);
							console.printMessage("Конвертация завершена!");
							console.printText("Посылаю запрос...");
							try {
								String response=request(flacfile);
                                                                console.printMessage("Запрос послан. Ответ получен!");
								console.printText("Ответ от сервера: "+response+"\n");		
                                                                String resultPhrase=new String(getParsedPhrase(response).getBytes("UTF-8"),"UTF-8"); //парсинг ответа на наличие фразы
                                                                console.printMessage("Вы сказали: "+resultPhrase);
                                                                launch(resultPhrase); //запуск команд, если такие распознаны
								listenButton.setEnabled(true);
							} catch (Exception e) {
								console.printError("Ошибка!\n"+e.toString());
								listenButton.setEnabled(true);
							}
						} catch (Exception e1) {
							console.printError("Ошибка конвертации!\n"+e1.toString());
							listenButton.setEnabled(true);
						}
					}
				}, (MAXSECONDS-1)*1000);		
				
				timer.scheduleAtFixedRate(new TimerTask() //выполняется каждую секунду
				{
					public void run()
					{
						if(counter==0)
						{
							console.printText("Запись пошла...");
							try {
								microphone.captureAudioToFile(file);
							} catch (Exception e) {
                                                                console.printError("Ошибка получения данных с микрофона.");
								console.printError(e.toString());
								listenButton.setEnabled(true);
							}
							microphone.open();
						}
						console.printText(String.valueOf(counter+1));		
						counter++;
					}
				}, 0, 1000);
			} 
			catch (Exception e1)
			{
				console.printError(e1.toString());
				listenButton.setEnabled(true);
			}
			
		}
		/////////////////////////////////////////////////////////////Нажатие кнопки добавить///////////////////////////////////
		if(e.getSource()==addButton)
		{
			if(command.getText()!="" && phrase.getText()!="")
			{
                            model.addElement(command.getText(),phrase.getText());
			}
		}
		/////////////////////////////////////////////////////////////Нажатие кнопки удалить///////////////////////////////////
		if(e.getSource()==deleteButton)
		{
			int[] rows=table.getSelectedRows();
			model.removeElement(rows);
		}
		/////////////////////////////////////////////////////////////Нажатие кнопки сохранить///////////////////////////////////
		if(e.getSource()==saveButton)
		{
			try {
				saveCommands();
				console.printMessage("XML-файл успешно сохранен!");
			} catch (Exception e1) {
				console.printError(e1.toString());
			}
		}
		if(e.getSource()==author)
		{
			JOptionPane.showMessageDialog(null, "Программа для распознавания голоса с помощью Google Speech API\n"
					+ "Программа позволяет запускать программы при произнесении определенной фразы,сохранение и загрузку списка фраз.\n"
					+ "Фразы и команды хранятся в XML-файле.\n"
					+ "В разработке были использованы: JavaFlacEncoder и Darkprograms.Speech.Microphone\n"
					+ "Программа создана Чудаевым Вячеславом в качестве курсовой работы по программированию на Java в МГТУ МИРЭА в 2013 году.\n"
                                        + "     Переделана под обновленную версию API v2 в октябре 2014 года.");
		}
		
		//////////////////////////////////////Нажатие радиокнопки///////////////////////////
		if(e.getSource()==ruslangButton)
		{
			language="ru-RU";
		}
		
		if(e.getSource()==englangButton)
		{
			language="en-US";
		}
		
		if(e.getSource()==gerlangButton)
		{
			language="de-DE";
		}
	}
	////////////////////////////////////////////////////////////////Конвертация во FLAC////////////////////////////////////////
	public void convertWaveToFlac(File inputFile, File outputFile) throws Exception 
        {
        StreamConfiguration streamConfiguration = new StreamConfiguration();
        streamConfiguration.setSampleRate(44100);
        streamConfiguration.setBitsPerSample(16);
        streamConfiguration.setChannelCount(1);

            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputFile);
            AudioFormat format = audioInputStream.getFormat();

            int frameSize = format.getFrameSize();

            FLACEncoder flacEncoder = new FLACEncoder();
            FLACFileOutputStream flacOutputStream = new FLACFileOutputStream(outputFile);

            flacEncoder.setStreamConfiguration(streamConfiguration);
            flacEncoder.setOutputStream(flacOutputStream);

            flacEncoder.openFLACStream();

            int[] sampleData = new int[(int) audioInputStream.getFrameLength()];
            byte[] samplesIn = new byte[frameSize];

            int i = 0;

            while (audioInputStream.read(samplesIn, 0, frameSize) != -1) {
                if (frameSize != 1) {
                    ByteBuffer bb = ByteBuffer.wrap(samplesIn);
                    bb.order(ByteOrder.LITTLE_ENDIAN);
                    short shortVal = bb.getShort();
                    sampleData[i] = shortVal;
                } else {
                    sampleData[i] = samplesIn[0];
                }
                i++;
            }
            flacEncoder.addSamples(sampleData, i);
            flacEncoder.encodeSamples(i, false);
            flacEncoder.encodeSamples(flacEncoder.samplesAvailableToEncode(), true);
            audioInputStream.close();
            flacOutputStream.close();
    }
	/////////////////////////////////////////////////////////////Отправка\получение запроса гугла///////////////////////////////////
	public String request(File inputFile) throws Exception
	{
		StringBuilder sb=new StringBuilder(googleurl);
                sb.append("output=json"); //формат вывода json
		sb.append("&lang="+language); //язык 
                sb.append("&key="+ApiKey); //API key
		URL url=new URL(sb.toString());
		URLConnection urlCon=url.openConnection();
		urlCon.setDoOutput(true);
		urlCon.setUseCaches(false);
		urlCon.setRequestProperty("Content-Type", "audio/x-flac; rate=44100");
		OutputStream outputStream=urlCon.getOutputStream();
		FileInputStream inputStream=new FileInputStream(inputFile);

		byte[] buffer=new byte[256];
		while(inputStream.read(buffer,0,256)!=-1)
                    outputStream.write(buffer,0,256);
		
		inputStream.close();
		outputStream.close();
		BufferedReader br=new BufferedReader(new InputStreamReader(urlCon.getInputStream(),Charset.forName("UTF-8")));
                
		String response="";
                String line;
                while((line=br.readLine())!=null)
                {
                    response+=line;
                }
                
		br.close();
		return response;
	}
	////////////////////////////////////////////////////////////////Парсинг JSON строки////////////////////////////////////////
	public String getParsedPhrase(String input)
	{
                int transcriptPos=input.indexOf("transcript")+13; //начало распознанной фразы
                int confidencePos=input.indexOf("confidence")-3; //конец распознанной фразы
                String res=input.substring(transcriptPos, confidencePos);
                return res;
	}
	/////////////////////////////////////////////////////////////Модель таблицы///////////////////////////////////
	public class CommandTableModel extends AbstractTableModel
	{
		private List<Command> coms;

		public CommandTableModel(List<Command> coms)
		{
			this.coms=new ArrayList<>(coms);
		}
		
		public int getColumnCount() {
			return 2;
		}

		public int getRowCount() {
			return coms.size();
		}

		public Object getValueAt(int row, int col) {
			Object value=null;
			switch(col)
			{
			case 0:
				value=coms.get(row).getPhrase();
				break;
			case 1:
				value= coms.get(row).getCommand();
				break;
			}
			return value;
		}
		
		public String getColumnName(int col)
		{
			String name="?";
			switch(col)
			{
			case 0:
				name="Фраза";
				break;
			case 1:
				name="Команда";
				break;
			}
			return name;
		}
		
		public void addElement(String text1,String text2)
		{	
			coms.add(new Command(text2,text1));
			this.fireTableDataChanged();
		}
		public void removeElement(int[] sel)
		{
			Arrays.sort(sel);
			for(int i=sel.length-1;i>=0;i--)
			{
				this.fireTableRowsDeleted(sel[i], sel[i]);
				this.coms.remove(sel[i]);
			}
		}
	}
	/////////////////////////////////////////////////////////////Запуск процесса///////////////////////////////////
	public void launch(String phrase)
	{
		List<Command> coms=model.coms;
		for(int i=0;i<coms.size();i++)
		{
			try {
				console.printMessage(coms.get(i).runCommand(phrase));
			} catch (Exception e) {
				console.printError(e.toString());
			}
		}
	}
	/////////////////////////////////////////////////////////////Сохранение///////////////////////////////////
	public void saveCommands() throws Exception
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		List<Command> list=model.coms;
		Document doc = docBuilder.newDocument();
		
		Element rootElement = doc.createElement("commands");
		doc.appendChild(rootElement);
		for(int i=0;i<list.size();i++)
		{
			Element com = doc.createElement("com");
			rootElement.appendChild(com);
	 
			Attr attr = doc.createAttribute("id");
			attr.setValue("i");
			com.setAttributeNode(attr);
	 
			Element command = doc.createElement("command");
			command.appendChild(doc.createTextNode(list.get(i).getCommand()));
			com.appendChild(command);

			Element phrase = doc.createElement("phrase");
			phrase.appendChild(doc.createTextNode(list.get(i).getPhrase()));
			com.appendChild(phrase);
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(commandsFile);
		transformer.transform(source, result);
	}
	/////////////////////////////////////////////////////////////Загрузка///////////////////////////////////
	public void loadCommands(File file) throws Exception
	{
		file=new File(XMLFilePath);
                DocumentBuilderFactory dbFac=DocumentBuilderFactory.newInstance();
                DocumentBuilder db=dbFac.newDocumentBuilder();
                InputStream inputStream=new FileInputStream(file);        
                Reader reader=new InputStreamReader(inputStream,"UTF-8");           
                InputSource is=new InputSource(reader);
                
                Document doc=db.parse(is);
		
                NodeList list=doc.getElementsByTagName("com");
                for(int i=0;i<list.getLength();i++)
                {
                    Node n=list.item(i);
                    if(n.getNodeType()==Node.ELEMENT_NODE)
                    {
                        Element e=(Element)n;
                        String com=new String(e.getElementsByTagName("command").item(0).getTextContent());
                        String ph=new String(e.getElementsByTagName("phrase").item(0).getTextContent());
                        model.addElement(com,ph);
                    }
                }
	}
}
