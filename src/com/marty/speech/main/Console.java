package com.marty.speech.main;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class Console extends JTextPane{
	private StyledDocument doc=this.getStyledDocument();
	public Console(int x,int y)
	{
		setEditable(false);
		setMaximumSize(new Dimension(x,y));
		setMinimumSize(new Dimension(x,y));
		setPreferredSize(new Dimension(x,y));
		
	}
	public void printText(String text) 
	{
		Style style=addStyle("Imma style",null);
		StyleConstants.setForeground(style, Color.black);
		try {
			doc.insertString(doc.getLength(), text+"\n", style);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printError(String error) 
	{
		Style style=addStyle("Imma style",null);
		StyleConstants.setForeground(style, Color.red);
		try {
			doc.insertString(doc.getLength(), error+"\n", style);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printMessage(String message)
	{
		Style style=addStyle("Imma style",null);
		StyleConstants.setForeground(style, Color.green);
		try {
			doc.insertString(doc.getLength(), message+"\n", style);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
